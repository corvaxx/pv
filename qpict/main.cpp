//*****************************************************************************
//*****************************************************************************

#include "directorytreemodel.h"
#include "directorypicturesmodel.h"
#include "iconimageprovider.h"
#include "fsimageprovider.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <QQmlComponent>
//#include <QQmlEngine>
//#include <QQmlContext>

//*****************************************************************************
//*****************************************************************************
void registerTypes()
{
    qmlRegisterType<DirectoryTreeModel>("qpict.model", 1, 0, "DirectoryTreeModel");
    qmlRegisterType<DirectoryPicturesModel>("qpict.model", 1, 0, "DirectoryPicturesModel");
}

//*****************************************************************************
//*****************************************************************************
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    registerTypes();

    QQmlApplicationEngine engine;
    engine.addImageProvider(IconImageProvider::name(), new IconImageProvider());
    engine.addImageProvider(FsImageProvider::name(),   new FsImageProvider());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
