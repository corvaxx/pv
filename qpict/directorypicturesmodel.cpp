//*****************************************************************************
//*****************************************************************************

#include "directorypicturesmodel.h"

#include <QDir>
#include <QFileInfo>
#include <QPixmap>

#include <assert.h>
#include <mutex>
#include <atomic>
#include <thread>

//*****************************************************************************
//*****************************************************************************
extern std::mutex pixCacheLocker;
extern QHash<QString, QPixmap> pixCache;

//*****************************************************************************
//*****************************************************************************
class DirectoryPicturesModel::Impl
{
    friend class DirectoryPicturesModel;

    DirectoryPicturesModel * m_parent;

    QString                  m_path;
    QStringList              m_files;

    QHash<int, QByteArray>   m_columns;

    std::atomic<bool>        m_stopPreloading;
    std::vector<std::thread> m_threads;

    Impl(DirectoryPicturesModel * parent);

    void preloadPictures(const QStringList &files);
    void stopPreloading();
    void preloadProc(const size_t idx, const QStringList files);

public:
    ~Impl();
};

//*****************************************************************************
//*****************************************************************************
DirectoryPicturesModel::Impl::Impl(DirectoryPicturesModel * parent)
    : m_parent(parent)
{
    m_columns[RoleId]          = "id";
    m_columns[RolePath]        = "path";
}

//*****************************************************************************
//*****************************************************************************
DirectoryPicturesModel::Impl::~Impl()
{
    stopPreloading();
}

//*****************************************************************************
//*****************************************************************************
void DirectoryPicturesModel::Impl::preloadPictures(const QStringList & files)
{
    m_stopPreloading = false;

    for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i)
    {
        m_threads.push_back(std::thread(&DirectoryPicturesModel::Impl::preloadProc, this, i, files));
    }
}

//*****************************************************************************
//*****************************************************************************
void DirectoryPicturesModel::Impl::stopPreloading()
{
    m_stopPreloading = true;
    for (size_t i = 0; i < m_threads.size(); ++i)
    {
        if (m_threads[i].joinable())
        {
            m_threads[i].join();
        }
    }
}

//*****************************************************************************
//*****************************************************************************
void DirectoryPicturesModel::Impl::preloadProc(const size_t idx, const QStringList files)
{
    for (size_t i = idx; i < files.size(); i += std::thread::hardware_concurrency())
    {
        if (m_stopPreloading)
        {
            break;
        }

        QString fileName = files[i];

        {
            std::lock_guard<std::mutex> lock(pixCacheLocker);
            if (pixCache.count(fileName) > 0)
            {
                QMetaObject::invokeMethod(m_parent, "dataLoaded", Qt::QueuedConnection, Q_ARG(QString, fileName));
                continue;
            }
        }

        QString ext = fileName.mid(fileName.lastIndexOf('.'));
        QPixmap pix = QPixmap(fileName, qPrintable(ext)).scaled(200, 200, Qt::KeepAspectRatio);

        {
            std::lock_guard<std::mutex> lock(pixCacheLocker);
            pixCache[fileName] = pix;
        }

        // emit m_parent->dataChanged(m_parent->index(i), m_parent->index(i));
        QMetaObject::invokeMethod(m_parent, "dataLoaded", Qt::QueuedConnection, Q_ARG(QString, fileName));
    }
}

//*****************************************************************************
//*****************************************************************************
DirectoryPicturesModel::DirectoryPicturesModel(QObject * parent)
    : QAbstractListModel(parent)
    , m_p(new Impl(this))
{

}

//*****************************************************************************
//*****************************************************************************
int DirectoryPicturesModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_p->m_files.size();
}

//*****************************************************************************
//*****************************************************************************
int DirectoryPicturesModel::columnCount(const QModelIndex & /*parent*/) const
{
    return m_p->m_columns.size();
}

//*****************************************************************************
//*****************************************************************************
QVariant DirectoryPicturesModel::data(const QModelIndex & index, int role) const
{
    int row = index.row();
    if (row >= m_p->m_files.size())
    {
        return QVariant();
    }

    switch (role)
    {
        case RoleId:
        {
            return row;
        }
        case RolePath:
        {
            return m_p->m_files[row];
        }
    }

    return QVariant();
}

//*****************************************************************************
//*****************************************************************************
QHash<int, QByteArray> DirectoryPicturesModel::roleNames() const
{
    return m_p->m_columns;
}

//*****************************************************************************
//*****************************************************************************
QString DirectoryPicturesModel::path() const
{
    return m_p->m_path;
}

//*****************************************************************************
//*****************************************************************************
void DirectoryPicturesModel::setPath(const QString & path)
{
    if (m_p->m_path == path)
    {
        return;
    }

    m_p->stopPreloading();

    m_p->m_path = path;
    emit pathChanged();

    QDir dir(path);

    QStringList filters;
    filters << "*.jpg" << "*.jpeg" << "*.png";

    emit beginResetModel();
    m_p->m_files.clear();
    emit endResetModel();
    emit lengthChanged();

    QFileInfoList list = dir.entryInfoList(filters, QDir::Files);
    QStringList files;
    std::transform(list.begin(), list.end(),
                   std::back_inserter(files),
                   [](const QFileInfo & i) { return i.filePath(); });


    if (list.size())
    {
        m_p->preloadPictures(files);
    }
}

//*****************************************************************************
//*****************************************************************************
int DirectoryPicturesModel::length() const
{
    return m_p->m_files.size();
}

//*****************************************************************************
//*****************************************************************************
void DirectoryPicturesModel::dataLoaded(const QString fileName)
{
    emit beginInsertRows(QModelIndex(), m_p->m_files.size(), m_p->m_files.size());
    m_p->m_files.push_back(fileName);
    emit endInsertRows();

    emit lengthChanged();
}
