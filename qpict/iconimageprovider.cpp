//*****************************************************************************
//*****************************************************************************

#include "iconimageprovider.h"

#include <QPixmap>
#include <QString>
#include <QDebug>
#include <QFileIconProvider>

//*****************************************************************************
//*****************************************************************************
class IconImageProvider::Impl
{
    friend class IconImageProvider;

    const static QString m_name;

    QFileIconProvider    m_prov;
};

//*****************************************************************************
//*****************************************************************************
// static
const QString IconImageProvider::Impl::m_name = QStringLiteral("icon");

//*****************************************************************************
//*****************************************************************************
IconImageProvider::IconImageProvider()
    : QQuickImageProvider(QQmlImageProviderBase::Pixmap)
    , m_p(new Impl)
{
}

//*****************************************************************************
//*****************************************************************************
QPixmap IconImageProvider::requestPixmap(const QString & id,
                                           QSize * size,
                                           const QSize & requestedSize)
{
    if (requestedSize.isValid())
    {
        *size = QSize(requestedSize.height(), requestedSize.height());
    }
    else
    {
        *size = QSize(32, 32);
    }

    QFileInfo info(id);
    QIcon icon = m_p->m_prov.icon(info);
    if (icon.isNull())
    {
        icon = m_p->m_prov.icon(QFileIconProvider::Drive);
    }
    return icon.pixmap(*size);
}

//*****************************************************************************
//*****************************************************************************
// static
QString IconImageProvider::name()
{
    return IconImageProvider::Impl::m_name;
}
