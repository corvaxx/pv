//*****************************************************************************
//*****************************************************************************

#include "directorytreemodel.h"

#include <QDir>
#include <QFileInfoList>

#include <vector>
#include <assert.h>

//*****************************************************************************
//*****************************************************************************
class DirectoryTreeModel::Impl
{
    friend class DirectoryTreeModel;

    std::vector<QFileInfo> m_items;

    struct Helper
    {
        bool     isExpanded;
        bool     hasChildren;
        uint32_t level;

        Helper()
            : isExpanded(false)
            , hasChildren(true)
            , level(0)
        {
        }
        Helper(const uint32_t level)
            : isExpanded(false)
            , level(level)
        {
        }
    };

    std::vector<Helper> m_helpers;

    QHash<int, QByteArray> m_columns;

    Impl();
};

//*****************************************************************************
//*****************************************************************************
DirectoryTreeModel::Impl::Impl()
{
    m_columns[RoleId]          = "id";
    m_columns[RoleName]        = "name";
    m_columns[RolePath]        = "path";
    m_columns[RoleLevel]       = "level";
    m_columns[RoleIsExpanded]  = "isExpanded";
    m_columns[RoleHasChildren] = "hasChildren";
}

//*****************************************************************************
//*****************************************************************************
DirectoryTreeModel::DirectoryTreeModel(QObject * parent)
    : QAbstractListModel(parent)
    , m_p(new Impl)
{
    QFileInfoList drives = QDir::drives();
    std::copy(drives.begin(), drives.end(), std::back_inserter(m_p->m_items));
    m_p->m_helpers.resize(drives.size());
}

//*****************************************************************************
//*****************************************************************************
int DirectoryTreeModel::rowCount(const QModelIndex & /*parent*/) const
{
    return m_p->m_items.size();
}

//*****************************************************************************
//*****************************************************************************
int DirectoryTreeModel::columnCount(const QModelIndex & /*parent*/) const
{
    return m_p->m_columns.size();
}

//*****************************************************************************
//*****************************************************************************
QVariant DirectoryTreeModel::data(const QModelIndex & index, int role) const
{
    size_t row = static_cast<size_t>(index.row());
    if (row >= m_p->m_items.size())
    {
        return QVariant();
    }

    const QFileInfo    & info = m_p->m_items[row];
    const Impl::Helper & hlp  = m_p->m_helpers[row];
    switch (role)
    {
        case RoleId:
        {
            return row;
        }
        case RoleName:
        {
            // TODO optimisation needed
            QString path = info.filePath();
            QStringList items = path.split("/", QString::SkipEmptyParts);
            return items.last();
        }
        case RolePath:
        {
            return info.filePath();
        }
        case RoleLevel:
        {
            return hlp.level;
        }
        case RoleIsExpanded:
        {
            return hlp.isExpanded;
        }
        case RoleHasChildren:
        {
            return hlp.hasChildren;
        }
    }

    return QVariant();
}

//*****************************************************************************
//*****************************************************************************
QHash<int, QByteArray> DirectoryTreeModel::roleNames() const
{
    return m_p->m_columns;
}

//*****************************************************************************
//*****************************************************************************
void DirectoryTreeModel::expand(const int idx)
{
    size_t row = static_cast<size_t>(idx);
    if (row >= m_p->m_items.size())
    {
        return;
    }

    const QFileInfo & info = m_p->m_items[row];
    Impl::Helper    & hlp  = m_p->m_helpers[row];

    if (hlp.isExpanded)
    {
        hlp.isExpanded = false;

        emit dataChanged(index(row), index(row));

        // collapse
        // calc count
        size_t count = 0;
        std::vector<Impl::Helper>::iterator up = m_p->m_helpers.begin() + row;
        for (std::vector<Impl::Helper>::iterator i = up+1; i != m_p->m_helpers.end() && up->level < i->level; ++i, ++count)
        {
        }

        if (count == 0)
        {
            return;
        }

        emit beginRemoveRows(QModelIndex(), row + 1, row + count);
        m_p->m_items.erase(m_p->m_items.begin() + row + 1, m_p->m_items.begin() + row + 1 + count);
        m_p->m_helpers.erase(m_p->m_helpers.begin() + row + 1, m_p->m_helpers.begin() + row + 1 + count);
        emit endRemoveRows();
    }
    else
    {
        hlp.isExpanded = true;

        // expand
        QDir dir(info.filePath());
        QFileInfoList dirs = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        if (!dirs.size())
        {
            hlp.isExpanded = hlp.hasChildren = false;
            emit dataChanged(index(row), index(row));

            return;
        }

        emit dataChanged(index(row), index(row));

        emit beginInsertRows(QModelIndex(), row + 1, row + dirs.size());
        m_p->m_items.insert(m_p->m_items.begin() + row + 1, dirs.begin(), dirs.end());
        m_p->m_helpers.insert(m_p->m_helpers.begin() + row + 1, dirs.size(), Impl::Helper(hlp.level + 1));
        emit endInsertRows();
    }

    assert(m_p->m_items.size() == m_p->m_helpers.size() && "bad helpers size");
}
