//*****************************************************************************
//*****************************************************************************

#ifndef DIRECTORYPICTURESMODEL_H
#define DIRECTORYPICTURESMODEL_H

#include <QAbstractListModel>

#include <memory>

//*****************************************************************************
//*****************************************************************************
class DirectoryPicturesModel : public QAbstractListModel
{
    class Impl;

    Q_OBJECT

    enum Role
    {
        RoleId = Qt::UserRole + 1,
        RolePath
    };

public:
    DirectoryPicturesModel(QObject * parent = nullptr);

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QHash<int, QByteArray> roleNames() const;

public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    QString path() const;
    void setPath(const QString & path);

    Q_PROPERTY(int length READ length NOTIFY lengthChanged)
    int length() const;

signals:
    void pathChanged();
    void lengthChanged();

private slots:
    void dataLoaded(const QString fileName);

private:
    std::shared_ptr<Impl> m_p;
};

#endif // DIRECTORYPICTURESMODEL_H
