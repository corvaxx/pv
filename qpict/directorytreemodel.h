//*****************************************************************************
//*****************************************************************************

#ifndef DIRECTORYTREEMODEL_H
#define DIRECTORYTREEMODEL_H

#include <QAbstractListModel>

#include <memory>

//*****************************************************************************
//*****************************************************************************
class DirectoryTreeModel : public QAbstractListModel
{
    class Impl;

    Q_OBJECT

    enum Role
    {
        RoleId = Qt::UserRole + 1,
        RoleName,
        RolePath,
        RoleLevel,
        RoleIsExpanded,
        RoleHasChildren
    };

public:
    DirectoryTreeModel(QObject * parent = nullptr);

public:
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

public:
    Q_INVOKABLE void expand(const int idx);

private:
    QHash<int, QByteArray> roleNames() const;

private:
    std::shared_ptr<Impl> m_p;
};

#endif // DIRECTORYTREEMODEL_H
