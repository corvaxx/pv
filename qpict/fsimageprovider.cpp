//*****************************************************************************
//*****************************************************************************

#include "fsimageprovider.h"

#include <QPixmap>
#include <QImage>
#include <QString>
#include <QDebug>
#include <QHash>

#include <mutex>

//*****************************************************************************
//*****************************************************************************
std::mutex pixCacheLocker;
QHash<QString, QPixmap> pixCache;

//*****************************************************************************
//*****************************************************************************
class FsImageProvider::Impl
{
    friend class FsImageProvider;

    const static QString m_name;
};

//*****************************************************************************
//*****************************************************************************
// static
const QString FsImageProvider::Impl::m_name = QStringLiteral("fs");

//*****************************************************************************
//*****************************************************************************
FsImageProvider::FsImageProvider()
    : QQuickImageProvider(QQmlImageProviderBase::Pixmap)
    , m_p(new Impl)
{
}

//*****************************************************************************
//*****************************************************************************
QPixmap FsImageProvider::requestPixmap(const QString & id,
                                       QSize * size,
                                       const QSize & requestedSize)
{
    if (requestedSize.isValid())
    {
        *size = QSize(requestedSize.height(), requestedSize.height());
    }
    else
    {
        *size = QSize(200, 200);
    }

//    if (pixCache.count(id) == 0)
//    {
//        QString ext = id.mid(id.lastIndexOf('.'));
//        pixCache[id] = QPixmap(id, qPrintable(ext)).scaled(200, 200, Qt::KeepAspectRatio);
//    }

    std::lock_guard<std::mutex> lock(pixCacheLocker);

    if (pixCache[id].isNull())
    {
        // TODO return loading placeholder
        return QPixmap();
    }
    return pixCache[id];
}

//*****************************************************************************
//*****************************************************************************
// static
QString FsImageProvider::name()
{
    return FsImageProvider::Impl::m_name;
}

