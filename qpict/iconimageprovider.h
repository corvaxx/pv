//*****************************************************************************
//*****************************************************************************

#ifndef ICONIMAGEPROVIDER_H
#define ICONIMAGEPROVIDER_H

#include <QQuickImageProvider>
#include <QQmlEngine>
#include <QPixmap>
#include <QString>

#include <memory>

//*****************************************************************************
//*****************************************************************************
class IconImageProvider : public QQuickImageProvider
{
    class Impl;

public:
    IconImageProvider();

    static QString name();

    QPixmap requestPixmap(const QString & id, QSize * size, const QSize & requestedSize);

private:
    std::unique_ptr<Impl> m_p;
};

#endif // ICONIMAGEPROVIDER_H
