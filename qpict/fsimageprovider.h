//*****************************************************************************
//*****************************************************************************

#ifndef FSIMAGEPROVIDER_H
#define FSIMAGEPROVIDER_H

#include <QQuickImageProvider>
#include <QQmlEngine>
#include <QPixmap>
#include <QString>

#include <memory>

//*****************************************************************************
//*****************************************************************************
class FsImageProvider : public QQuickImageProvider
{
    class Impl;

public:
    FsImageProvider();

    static QString name();

    QPixmap requestPixmap(const QString & id, QSize * size, const QSize & requestedSize);

private:
    std::unique_ptr<Impl> m_p;
};

#endif // FSIMAGEPROVIDER_H
