import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

import "colors.js" as Colors
import "components"
import "controls"
import "views"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello Picture Viewer")

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        handleDelegate: VerticalSplitterDelegate {
            color: Colors.bgColor
        }

        DirectoryTree2 {
            width: 200
            // Layout.maximumWidth: 400
            Layout.minimumWidth: 50
            border {
                width: 1
                color: Colors.borderColor
            }

            color: Colors.defaultColor

            onDirectoryChanged: {
                idPicturesList.path = directory;
            }
        }

        PicturesList {
            id: idPicturesList
            Layout.minimumWidth: 50
            Layout.fillWidth: true
            border {
                width: 1
                color: Colors.borderColor
            }
            color: Colors.defaultColor
        }
    }

    statusBar: StatusBar {}
}
