import QtQuick 2.0

import "../colors.js" as Colors

import qpict.model 1.0

Rectangle {
    id: idPicturesPanel

    clip: true;

    property alias path: idModel.path

    property real __cellSize: 200

    FocusScope {

        anchors.fill: parent

        DirectoryPicturesModel {
            id: idModel
        }

        Component {
            id: idDelegate

            Rectangle {
                width: __cellSize
                height: __cellSize

                border {
                    width: 1
                    color: Colors.borderColor
                }

//                Text {
//                    anchors.centerIn: parent
//                    text: model.path
//                }
                Image {
                    anchors.fill: parent
                    source: "image://fs/" + model.path
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit
                }
            }
        }

        GridView {
            id: idGrid
            anchors.fill: parent
            anchors.margins: 4
            cellWidth: __cellSize+4
            cellHeight: __cellSize+4
            model: idModel
            delegate: idDelegate
            cacheBuffer: 4
            visible: false
        }

        Text {
            id: idEmpty
            text: qsTr("no images found")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }

        MouseArea {
            anchors.fill: parent
            onWheel: {
                if (wheel.angleDelta.y < 0)
                {
                    if (__cellSize > 50)
                    {
                        // __cellSize /= 2;
                        __cellSize -= 25;
                    }
                }
                else
                {
                    if (__cellSize < 200)
                    {
                        // __cellSize *= 2;
                        __cellSize += 25;
                    }
                }
            }
        } // MouseArea

        states: [
            State {
                name: "empty"
                when: idModel.length === 0
                PropertyChanges { target: idGrid; visible: false; }
                PropertyChanges { target: idEmpty; visible: true; }
            },
            State {
                name: "items"
                when: idModel.length > 0
                PropertyChanges { target: idGrid; visible: true; }
                PropertyChanges { target: idEmpty; visible: false; }
            }
        ]

    } // FocusScope
} // Rectangle
