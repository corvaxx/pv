import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

import "../controls"

import qpict.model 1.0

Rectangle {
    FocusScope {

        id: idDirListScope

        anchors.fill: parent

        DirectoryTreeModel {
            id: idDirModel
        }

        TreeView {
            id: idDirList

            anchors.fill: parent
            anchors.margins: -1

            alternatingRowColors: false

            model: idDirModel

            // headerDelegate : Item {}

            TableViewColumn {
                title: ""
                role: "filePath"
                width: 32

                delegate: Image {
                    width: 32
                    height: 32
                    source: "image://fs/" + styleData.value
                }
            }

            TableViewColumn {
                id: idNameColumn
                title: "Name"
                role: "fileName"
                width: 300

                delegate: Rectangle {
                    // width: idNameColumn.width
                    height: 32
                    Text {
                        anchors.fill: parent
                        text: styleData.value
                    }
                }
            }
        }

//        QPictScrollView {

//            id: idDirFoldersListHolder

//            anchors {
//                fill: parent
//                margins: 2
//            }

////            Component {

////                id: idFoldersListItem

////                CustomBorderRectangle2 {

////                    id: idFoldersListDelegate
////                    objectName: "foldersListDelegate"
////                    property int itemIndex: model.index

////                    property bool isCurrent: ListView.isCurrentItem

////                    property variant list: idFolderListDelegateList

////                    width: parent.width;
////                    height: childrenRect.height

////                    state: "expanded"

////                    CustomBorderRectangle2 {

////                        id: idFolderListDelegateMain

////                        anchors.left: parent.left
////                        anchors.right: parent.right
////                        height: idFolderImage.height + 16

////                        property int borderWidth: parent.isCurrent ? 1 : 0 // (idItems.currentIndex == index) ? 1 : 0
////                        borderTop: borderWidth
////                        borderLeft: borderWidth
////                        borderRight: borderWidth
////                        borderBottom: borderWidth

////                        Image {
////                            id: idFolderImage

////                            anchors.left: parent.left
////                            anchors.leftMargin: width
////                            anchors.verticalCenter: parent.verticalCenter
////                            source: imagePath
////                        }
////                        MessengerText {
////                            id: idFolderText
////                            anchors.left: idFolderImage.right
////                            anchors.leftMargin: idFolderImage.width/2
////                            anchors.verticalCenter: idFolderImage.verticalCenter
////                            text: name + (unreadCount ? ("(" + unreadCount +  ")") : "")
////                        }

////                    } // idFolderListDelegateMain

////                    MessengerListView {
////                        id: idFolderListDelegateList
////                        interactive: false
////                        // visible: false

////                        anchors.top: idFolderListDelegateMain.bottom
////                        anchors.leftMargin: idFolderImage.width
////                        anchors.left: parent.left
////                        anchors.right: parent.right
////                        // height: visible ? childrenRect.height : 0
////                        height: childrenRect.height

////                        model: folders

////                        delegate: idFoldersListItem
////                        delegateObjectName: "foldersListDelegate"

////                    } // ListView

////                    states: [
////                        State {
////                            name: "expanded"
////                            PropertyChanges { target: idFolderListDelegateList; visible: true; height: childrenRect.height }
////                        },
////                        State {
////                            name: "collapsed"
////                            PropertyChanges { target: idFolderListDelegateList; visible: false; height: 0 }
////                        }
////                    ]

////                }

////            } // idFoldersListItem

//            QPictListView {

//                id: idDirList

//        //        Image { source: "../images/small_steps.png";
//        //            fillMode: Image.Tile;
//        //            anchors.fill: parent;
//        //            opacity: 0.3

//        //        }

//                anchors {
//                    fill: parent
//                    margins: 2
//                }

////                model: EmailFoldersModel {}

////                delegate: idFoldersListItem
////                delegateObjectName: "foldersListDelegate"

////                MouseArea {
////                    anchors.fill: parent
////                    onClicked: {

////                        var x = mouseX;
////                        var y = idEmailFoldersListHolder.flickableItem.contentY+mouseY;

////                        var list = idFoldersList;

////                        while (list) {

////                            var index = list.indexAt(x, y);
////                            console.log(index);
////                            list.currentIndex = index;

////                            var item = item.delegateAt(index);
////                            if (item) {
////                                // var o = list.mapFromItem(, x, y);
////                                list = item.list;
////                            }
////                        }
////                    }
////                }

//            } // ListView
//        } // ScrollView
    } // focus scope

//    Keys.onPressed: {
//        if (idDirListScope.focus && (event.key === Qt.Key_Down || event.key === Qt.Key_Up)) {
//            console.log(idDirList.currentIndex);
//        }
//    }

} // rectangle
