import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

import "../controls"
import "../colors.js" as Colors

import qpict.model 1.0

Rectangle {

    id: idDirPanel

    signal directoryChanged(var directory)

    FocusScope {

        id: idDirListScope

        anchors.fill: parent

        DirectoryTreeModel {
            id: idDirModel
        }

        Component {

            id: idFoldersListItem

            CustomBorderRectangle2 {

                id: idFoldersListDelegate

                property bool isCurrent: ListView.isCurrentItem

                width: idDirList.width;
                height: childrenRect.height

                CustomBorderRectangle2 {

                    id: idFolderListDelegateMain

                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: idFolderImage.height

                    property int borderWidth: parent.isCurrent ? 1 : 0 // (idItems.currentIndex == index) ? 1 : 0
                    borderTop: borderWidth
                    borderLeft: borderWidth
                    borderRight: borderWidth
                    borderBottom: borderWidth

                    Image {
                        id: idFolderImage

                        width: 32
                        height: 32

                        anchors.left: parent.left
                        anchors.leftMargin: 4 + width * model.level
                        anchors.verticalCenter: parent.verticalCenter
                        source: "image://icon/" + model.path
                    }

                    Text {
                        id: idFolderText
                        anchors {
                            left: idFolderImage.right
                            leftMargin: 4
                            right: parent.right
                            bottom: idFolderImage.bottom
                            bottomMargin: 4
                        }
                        color: Colors.contrastColor
                        text: model.name
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            idDirList.currentIndex = model.index;
                            idDirPanel.directoryChanged(model.path);
                        }

                        onDoubleClicked: {
                            idDirModel.expand(model.id);
                        }
                    }
                } // idFolderListDelegateMain
            }
        } // idFoldersListItem

        QPictScrollView {

            id: idListHolder

            anchors {
                fill: parent
                margins: 2
            }

            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

            QPictListView {

                id: idDirList

                width: idDirPanel.width
                height: childrenRect.height

                model: idDirModel

                delegate: idFoldersListItem

            } // ListView
        } // ScrollView
    } // focus scope
} // rectangle
