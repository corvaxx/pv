import QtQuick 2.0

import "../controls"
import "../colors.js" as Colors

CustomBorderRectangle2 {

    width: parent.width
    height: 24

    color: Colors.bgColor

    Text {
        text: qsTr("Footer here")
        anchors.centerIn: parent
        color: "black"
    }
}
