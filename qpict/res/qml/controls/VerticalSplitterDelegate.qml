import QtQuick 2.0
import QtQuick.Layouts 1.0

Rectangle {

    id: idHandler

    property int dotSize: 3
    property color dotColor: "darkgray"

    width: dotSize*2 // idSplitterView.width
    height: dotSize*2

    ColumnLayout {

        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        Item {
            Layout.fillHeight: true
        }
        Rectangle {
            width: idHandler.dotSize
            height: idHandler.dotSize
            radius: idHandler.dotSize
            color: idHandler.dotColor
        }
        Rectangle {
            width: idHandler.dotSize
            height: idHandler.dotSize
            radius: idHandler.dotSize
            color: idHandler.dotColor
        }
        Rectangle {
            width: idHandler.dotSize
            height: idHandler.dotSize
            radius: idHandler.dotSize
            color: idHandler.dotColor
        }
        Item {
            Layout.fillHeight: true
        }
    }
}
