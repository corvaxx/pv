import QtQuick 2.0

Item {

    id: idComponent

    property int borderTop: 0
    property int borderLeft: 0
    property int borderRight: 0
    property int borderBottom: 0

    property int contentMargins: 0

    property color color: "white"
    property color borderColor: "black"

    Rectangle {
        id: idBorder

        anchors.fill: parent
        anchors.margins: idComponent.contentMargins

        color: idComponent.borderColor

        // border.width: 1
        // border.color: "green"

        Rectangle {
            id: idContent

            color: idComponent.color

            anchors.fill: parent
            anchors.leftMargin: idComponent.borderLeft
            anchors.rightMargin: idComponent.borderRight
            anchors.topMargin: idComponent.borderTop
            anchors.bottomMargin: idComponent.borderBottom
        }
    }
}
