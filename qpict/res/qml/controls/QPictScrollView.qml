import QtQuick 2.0
import QtQuick.Controls 1.0

ScrollView {
    Component.onCompleted: {
        if (flickableItem) {
            flickableItem.boundsBehavior = Flickable.StopAtBounds;
        }
    }
}
