import QtQuick 2.0

ListView {

    id: idList

    // properties for delegateAt(index)
    // objectName: "delegateObjectName"
    // property int itemIndex: model.index

    property string delegateObjectName: ""

    function delegateAt(index) {
        for(var i = 0; i < contentItem.children.length; ++i) {
            var item = contentItem.children[i];
            if (item.objectName === delegateObjectName && item.itemIndex === index) {
                return item;
            }
        }
        return undefined;
    }

    Component.onCompleted: {
        // idList.currentIndex = count-1;
        idList.currentIndex = -1
    }
}
